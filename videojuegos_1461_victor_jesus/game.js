// Initialise Phaser
var game = new Phaser.Game(1366, 768, Phaser.AUTO, 'gameDiv');
game.sounds = new Sound();


text_intro  = "Hola, le damos de nuevo la bienvenida al centro de desarrollo computerizado de Aperture Science. Esperamos que su breve estancia en la camara de relajacion fuese de su agrado. Se ha procesado su especimen, comencemos ahora con la prueba en si.";
tpwr1       = "Enhorabuena, ha sabido utilizar el dispositivo portatil de portales de Aperture Science. Como parte del protocolo de prueba, no sera supervisada en las siguientes camaras. Estara completamente sola. Suerte!";
tpwr2       = "Como parte del protocolo de prueba, la anterior afirmacion de que no la supervisariamos era totalmente falsa. Las siguientes camaras no seran tan sencillas. Si las supera se le servira un trozo de tarta.";
tpwr3       = "El centro se asegura de que todas las pruebas son posibles. Salvo la siguiente...";
tpend1      = "Enhorabuena, has superado las pruebas. El equipo de Aperture soporta temperaturas de hasta cuatro mil grados Kelvin. No se preocupe, es del todo imposible que se produzca una averia peligrosa antes de que le envuelvan las llamas de la gloria.";
tpend2      =  "Gracias por participar en las actividades de desarrollo computerizado de Aperture Science. Adios."

// Add all the states
game.state.add('boot',    new BootState(game, 'boot'));
game.state.add('load',    new LoadState(game, 'load'));
game.state.add('menu',    new MenuState(game, 'menu'));
game.state.add('options', new OptionState(game, 'options'));
game.state.add('control', new ControlState(game, 'control'));
game.state.add('about',   new AboutState(game, 'about'));
game.state.add('win',     new MenuState(game, 'win'));

game.state.add('intro',    new TypeWriterState(game, 'intro', 'play1', text_intro));
game.state.add('play1',    new LevelState(game, 'play1', 1, 'tpwr1', 'mapa1'));

game.state.add('tpwr1',    new TypeWriterState(game, 'tpwr1', 'play2', tpwr1));
game.state.add('play2',    new LevelState(game, 'play2', 2, 'play3', 'mapa2'));
game.state.add('play3',    new LevelState(game, 'play3', 2, 'play4', 'mapa3'));
game.state.add('play4',    new LevelState(game, 'play4', 2, 'play5', 'mapa4'));
game.state.add('play5',    new LevelState(game, 'play5', 2, 'tpwr2', 'mapa5'));

game.state.add('tpwr2',    new TypeWriterState(game, 'tpwr2', 'play6', tpwr2));
game.state.add('play6',    new LevelState(game, 'play6', 3, 'play7', 'mapa6'));
game.state.add('play7',    new LevelState(game, 'play7', 3, 'play8', 'mapa7'));
game.state.add('play8',    new LevelState(game, 'play8', 4, 'tpwr3', 'mapa8'));


game.state.add('tpwr3',    new TypeWriterState(game, 'tpwr3', 'play9', tpwr3));
game.state.add('play9',    new LevelState(game, 'play9', 4, 'play10', 'mapa9'));
game.state.add('play10',   new LevelState(game, 'play10', 5, 'tpend1', 'mapa10'));
game.state.add('tpend1',   new TypeWriterState(game, 'tpend1', 'tpend2', tpend1));
game.state.add('tpend2',   new TypeWriterState(game, 'tpend2', 'menu', tpend2));

// Start the 'boot' state
game.state.start('boot');