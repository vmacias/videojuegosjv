function Portals(game, portalLeftColor,portalRightColor) {
	this.game = game;

	this.portalLeftColor  = portalLeftColor;
	this.portalRightColor = portalRightColor;
}

Portals.prototype = {
	create: function() {
		this.portal_left  = this.createPortal(this.portalLeftColor);
		this.portal_right = this.createPortal(this.portalRightColor);
	},

	createPortal: function(color) {
		var portal = this.game.add.sprite(0,0,'portal');
		this.game.physics.arcade.enable(portal);
		portal.tint = color;
		portal.visible = false;

		//añadir propiedades
		//portal.anchor.setTo(0.5,0.5);
		return portal;
	},

	setLeftPortal: function(x,y) {
		this.portal_left.x = x;
		this.portal_left.y = y;
		this.portal_left.visible = true;
		this.game.sounds.play_game_sound('portal_open');
	},

	setRightPortal: function(x,y) {
		this.portal_right.x = x;
		this.portal_right.y = y;
		this.portal_right.visible = true;
		this.game.sounds.play_game_sound('portal_open');
	},

	disableLeftPortal: function() {
		this.portal_left.visible = false;
	},

	disableRightPortal: function() {
		this.portal_right.visible = false;
	},

	rotatePortal: function(sprite, objectPosition) {
		if (objectPosition.up) {
			sprite.angle = 270;
			sprite.anchor.setTo(1,0.5);
			sprite.body.setSize(32,10,20,69);
			return {up:true,down:false,left:false,right:false,x_axis:true}
		} else if (objectPosition.down) {
			sprite.angle = 90;
			sprite.anchor.setTo(0.75,0.5);
			sprite.body.setSize(32,10,6,79);
			return {up:false,down:true,left:false,right:false,x_axis:true}
		} else if (objectPosition.left) {
			sprite.angle = 0;
			sprite.body.setSize(10,32,-6,60);
			sprite.anchor.setTo(0,0.5);
			return {up:false,down:false,left:true,right:false,x_axis:false}
		} else if (objectPosition.right) {
			sprite.angle = 180;
			sprite.anchor.setTo(0.75,0.5);
			sprite.body.setSize(10,32,42,60);
			return {up:false,down:false,left:false,right:true,x_axis:false}
		}
	},

	rotateLeftPortal: function (objectPosition) {
		this.portal_left_pos = this.rotatePortal(this.portal_left, objectPosition);
	},

	rotateRightPortal: function (objectPosition) {
		this.portal_right_pos = this.rotatePortal(this.portal_right, objectPosition);
	},

	sprite_through_left_portal: function (sprite) {
        if (this.portal_left_pos != null && this.portal_right_pos != null) {
            sprite.x = this.portal_right.x;
            sprite.y = this.portal_right.y;
            this.sprite_through_portal(sprite, this.portal_left_pos, this.portal_right_pos, this.portal_right.x, this.portal_right.y);
        }
	},

	sprite_through_right_portal: function (sprite) {
        if (this.portal_left_pos != null && this.portal_right_pos != null) {
            sprite.x = this.portal_left.x;
            sprite.y = this.portal_left.y;
            this.sprite_through_portal(sprite, this.portal_right_pos, this.portal_left_pos, this.portal_left.x, this.portal_left.y);
        }
	},

	sprite_through_portal: function (sprite, portal_ini_pos, portal_end_pos, end_x, end_y) {
		var velocity_x = 0;
		var velocity_y = 0;

		/*if (portal_end_pos.up) {
			sprite.body.velocity.y = 0;
		} else if (portal_end_pos.down) {
			sprite.body.velocity.y = -100;
		} else if (portal_end_pos.left) {
			sprite.body.velocity.x = 100;
		} else if (portal_end_pos.right) {
			sprite.body.velocity.x = -100;
		}*/

        var sprite_x = end_x;
		var sprite_y = end_y;

        
        var angle = this.angle_between_portals(portal_ini_pos, portal_end_pos);
        
        if (angle === 0) {
            if (Math.sin(this.angle_portal(portal_ini_pos)) === 0) {
                velocity_x = (-1) * sprite.body.velocity.x;
                velocity_y = sprite.body.velocity.y;
            } else {
                velocity_x = sprite.body.velocity.x;
                velocity_y = (-1) * sprite.body.velocity.y;
            }
        } else if (angle === 90) {
            velocity_x = (-1) * sprite.body.velocity.y;
            velocity_y = sprite.body.velocity.x;
        } else if (angle === 180) {
            velocity_x = sprite.body.velocity.x;
            velocity_y = sprite.body.velocity.y;
        } else if (angle === 270) {
            velocity_x = sprite.body.velocity.y;
            velocity_y = (-1) * sprite.body.velocity.x;
        }
        
		// same axis
		if (portal_ini_pos.x_axis === portal_end_pos.x_axis) {
			// both x axis
			if (portal_ini_pos.x_axis) {
				velocity_x = sprite.body.velocity.x;
				velocity_y = sprite.body.velocity.y;
				if (portal_ini_pos.up === portal_end_pos.up) {
					velocity_y = -sprite.body.velocity.y;
				}
			}

			// both y axis
			else {
				velocity_y = sprite.body.velocity.y;
				velocity_x = sprite.body.velocity.x;
				if (portal_ini_pos.left === portal_end_pos.left) {
					velocity_x = -sprite.body.velocity.x;
				}
			}
		}

		// different axis
		else {
			if (portal_ini_pos.x_axis) {
				velocity_x = Math.abs(sprite.body.velocity.y);
				velocity_y = sprite.body.velocity.x;
			} else {
				velocity_y = Math.abs(sprite.body.velocity.x);
				velocity_x = sprite.body.velocity.y;
			}
		}

        if (portal_end_pos.up) {
            sprite_y += 50;
			velocity_y += 100;
		} else if (portal_end_pos.down) {
            sprite_y += -130;
			velocity_y += -100;
		} else if (portal_end_pos.left) {
            sprite_x += 50;
			velocity_x += 100;
		} else if (portal_end_pos.right) {
            sprite_x += -50;
			velocity_x += -100;
        }
        
        sprite.x = sprite_x;
		sprite.y = sprite_y;
    
		sprite.body.velocity.x = velocity_x;
		sprite.body.velocity.y = velocity_y;

		this.game.sounds.play_game_sound('portal_enter');
	},
    
    angle_between_portals: function (portal1, portal2) {
        return this.angle_portal(portal2) - this.angle_portal(portal1);
    },
    
    angle_portal: function(portal) {
        var angle = 0;
        if (portal.up) {
            angle = 270;
		} else if (portal.down) {
            angle = 90;
		} else if (portal.left) {
            angle = 0;
		} else if (portal.right) {
            angle = 180;
        }
        return angle;
    }

}

/*
sprite_through_left_portal: function (sprite) {
		sprite.x = this.portal_right.x;
		sprite.y = this.portal_right.y;
		this.sprite_through_portal(sprite, this.portal_left_pos, this.portal_right_pos);
	},

	sprite_through_right_portal: function (sprite) {
		sprite.x = this.portal_left.x;
		sprite.y = this.portal_left.y;
		this.sprite_through_portal(sprite, this.portal_right_pos, this.portal_left_pos);
	},

	sprite_through_portal: function (sprite, portal_ini_pos, portal_end_pos) {
		var velocity_x = 0;
		var velocity_y = 0;

		if (portal_end_pos.up) {
			sprite.body.velocity.y = 0;
		} else if (portal_end_pos.down) {
			sprite.body.velocity.y = -100;
		} else if (portal_end_pos.left) {
			sprite.body.velocity.x = 100;
		} else if (portal_end_pos.right) {
			sprite.body.velocity.x = -100;
		}

		// same axis
		if (portal_ini_pos.x_axis === portal_end_pos.x_axis) {
			// both x axis
			if (portal_ini_pos.x_axis) {
				velocity_x = sprite.body.velocity.x;
				velocity_y = sprite.body.velocity.y;
				if (portal_ini_pos.up === portal_end_pos.up) {
					velocity_y = -sprite.body.velocity.y;
				}
			}

			// both y axis
			else {
				velocity_y = sprite.body.velocity.y;
				velocity_x = sprite.body.velocity.x;
				if (portal_ini_pos.left === portal_end_pos.left) {
					velocity_x = -sprite.body.velocity.x;
				}
			}
		}

		// different axis
		else {
			if (portal_ini_pos.x_axis) {
				velocity_x = Math.abs(sprite.body.velocity.y);
				velocity_y = sprite.body.velocity.x;
			} else {
				velocity_y = Math.abs(sprite.body.velocity.x);
				velocity_x = sprite.body.velocity.y;
			}
		}

		sprite.body.velocity.x = velocity_x;
		sprite.body.velocity.y = velocity_y;

		this.game.sounds.play_game_sound('portal_enter');
	}

}*/