function Player(game, layer_collision, actions, levelStart, context){
	Phaser.Sprite.call(this, game, levelStart.start_x, levelStart.start_y , 'player');

	// New variables
	this.layer_collision = layer_collision;
	this.actions = actions;
	this.context = context;
	this.scalep  = 0.25;

	// Add sprite to game
	game.add.existing(this);

	// Player Children
	this.head      = game.make.sprite(45-77,5-30,'head');
	this.arm       = game.make.sprite(12-77,0,'arm');
	this.empty_arm = game.make.sprite(0,0,'empty_arm'); // meh, pero hay que hacerlo aqui!

		// Anchor
	this.anchor.setTo(0.5, 30/350);
	this.head.anchor.setTo(0.542, 0.846);
	this.arm.anchor.setTo(0.148, 0.168);
	this.empty_arm.anchor.setTo(0.148, 0.168);

		// Add children
	this.addChild(this.head);
	this.addChild(this.arm);
	this.addChild(this.empty_arm);

	// Physics
	game.physics.arcade.enable(this);
	this.body.setSize(220,460,-32,-80);
	this.scale.setTo(this.scalep, this.scalep);
	this.body.gravity.y          = 1800;
	this.body.maxVelocity.y      = 1000;
	this.body.collideWorldBounds = true;

	this.visible = false;

	// Camera
	game.camera.follow(this);

	// Animations
	this.animations.add('run',[0,3,2,1], 15, true);

	this.jumpTimer = 0;

	this.trackedItem = null;
	this.lasttrack   = this.game.time.now;
	this.delaytrack  = 2000;

	levelStart.animationIni(this)
}

Player.prototype = Object.create(Phaser.Sprite.prototype);

Player.prototype.constructor = Player;

// functions
Player.prototype.update = function() {
	this.updaterotation();
	this.updateposition();

	if (this.trackedItem) {
		this.trackedItem.x = this.x;
		this.trackedItem.y = this.y;		
	}
}

Player.prototype.updaterotation = function() {
	var atanrad  = Math.atan2(game.input.mousePointer.y + this.game.camera.y - this.y, game.input.mousePointer.x + this.game.camera.x - this.x);
	var angle    = 180*atanrad/Math.PI;
	var quadrant = Math.floor(atanrad/Math.PI*2);

	/**
	* left up    -> -2
	* right up   -> -1
	* right down ->  0
	* left down  ->  1
	*/
	this.empty_arm.angle = angle;
	if (quadrant == -1) {
		this.scale.setTo(this.scalep, this.scalep);
		this.arm.angle  = angle;
		this.head.angle = angle/2;
		this.rotateItem(atanrad);
	} else if (quadrant == 0) {
		this.scale.setTo(this.scalep, this.scalep);
		this.arm.angle  = angle/2;
		this.head.angle = angle/4;
		this.rotateItem(0);
	} else if (quadrant == -2) {
		this.scale.setTo(-this.scalep, this.scalep);
		this.arm.angle  = 180-angle;
		this.head.angle = -90-angle/2;
		this.rotateItem(atanrad)
	} else {
		this.scale.setTo(-this.scalep, this.scalep);
		this.arm.angle  = 90-angle/2;
		this.head.angle = 45-angle/4;
		this.rotateItem(Math.PI)
		//this.rotateItem(90-angle, -1);
	}
}

Player.prototype.updateposition = function () {
	// Move the player to the left
	if (this.actions.left.isDown) {
		this.body.velocity.x = -500;
		if (this.body.onFloor() || this.body.touching.down) {
			this.animations.play('run'); // Start the left animation 
			this.game.sounds.play_player_step();
		} else {
			this.animations.stop();
		}
	}
	// Move the player to the right
	else if (this.actions.right.isDown) {
		this.body.velocity.x = 500;
		if (this.body.onFloor() || this.body.touching.down) {
			this.animations.play('run'); // Start the right animation
			this.game.sounds.play_player_step();
		} else {
			this.animations.stop();
		}
	}
	// Stop the player
	else {
		this.body.velocity.x = 0;
		this.animations.stop(); // Stop the animation
		this.frame = 0; // Set the player frame to 4 (stand still)
		//this.frame = 4; // Set the player frame to 4 (stand still)
	}
	// Make the player jump
	if (this.actions.jump.isDown && (this.body.onFloor() || this.body.touching.down) /*&& game.time.now > this.jumpTimer*/) {
		this.body.velocity.y = -450;
		/*this.jumpTimer = game.time.now + 500;*/
		//this.jumpSound.play();
	}

	if (this.trackedItem) {
		this.trackedItem.body.velocity.x = this.body.velocity.x;
		this.trackedItem.body.velocity.y = this.body.velocity.y;		
	}
}

Player.prototype.catchItem = function (player, item) {
	if (player.trackedItem === null) {
		player.trackedItem = item;
	}
}

Player.prototype.dropItem = function () {
	if (this.trackedItem) {
		this.trackedItem = null;
		return true;
	}
	return false;
}

Player.prototype.rotateItem = function (angle) {
	if (this.trackedItem) {
		this.trackedItem.rotation = angle;
	}
}