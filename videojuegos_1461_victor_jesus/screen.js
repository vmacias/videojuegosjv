function Screen(game) {
	this.game = game;
}

Screen.ChangeResolution = function(x,y,param) {
	if (param === 'fullScreen') {
		this.game.scale.startFullScreen(false);
	} else {
		this.game.scale.setGameSize(x,y);
	}
}

//Screen.changeResolution('800x600');