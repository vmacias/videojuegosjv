function Action() {
    this.actions = {};
}

Action.DefaultMenuActions = function () {
    return {
        "up":    {"name": "up",    "type": "keyboard", "value": Phaser.KeyCode.W},
        "down":  {"name": "down",  "type": "keyboard", "value": Phaser.KeyCode.S},
        "enter": {"name": "enter", "type": "keyboard", "value": Phaser.KeyCode.F},
        "back":  {"name": "back",  "type": "keyboard", "value": 32}
    };
};

Action.DefaultGameActions = function () {
    return {
        "left":      {"name": "left",      "type": "keyboard", "value": Phaser.KeyCode.A},
        "right":     {"name": "right",     "type": "keyboard", "value": Phaser.KeyCode.D},
        "jump":      {"name": "jump",      "type": "keyboard", "value": 32},
        "action":    {"name": "action",    "type": "keyboard", "value": Phaser.KeyCode.F},
        "fireleft":  {"name": "fireleft",  "type": "mouse",    "value": "mouseleft"},
        "fireright": {"name": "fireright", "type": "mouse",    "value": "mouseright"},
        "pause":     {"name": "pause",     "type": "keyboard", "value": Phaser.KeyCode.P}
    };
};

Action.getGameActions = function () {
	return JSON.parse(localStorage.getItem('gameAction_keys'));
}

Action.getMenuActions = function () {
	return JSON.parse(localStorage.getItem('menuAction_keys'));
}



Action.prototype = {
	_clearAction: function(action_key) {
		if (action_key.type === 'keyboard') {
			game.input.keyboard.removeKey(this.actions[action_key.name].KeyCode);
			this.actions[action_key.name] = {};
		}
	},

	_clearActions: function(action_keys) {
		for (action in action_keys) {
			this._clearAction(action_keys[action]);
		}

		this.actions = {};
	},

    _changeAction: function(action, action_keys_string, cond) {        
        var action_keys = JSON.parse(localStorage.getItem(action_keys_string));
        
		if (this.actualActions === cond ) {
            if (action_keys[action.name].type === 'keyboard') {
                game.input.keyboard.removeKey(action_keys[action.name].value);
            }

            this._setAction(action);
        }
        
        action_keys[action.name] = action;
        
        localStorage.setItem(action_keys_string, JSON.stringify(action_keys));
	},
    
	//estas dos juntarlas para modificar el js que lo contenga
	changeGameAction: function(name, type, value, display) {
		var action = {"name": name,  "type": type, "value": value, "display": display}
		this._changeAction(action, "gameAction_keys", "game");
	},

	changeMenuAction: function(action) {
        this._changeAction(action, "menuAction_keys", "menu");
	},

	_setAction: function(action) {
		if (action.type === 'keyboard') {
			this.actions[action.name] = game.input.keyboard.addKey(action.value);
		} else if (action.type === 'mouse') {
			if (action.value === 'mouseleft') {
				this.actions[action.name] = game.input.activePointer.leftButton;
			} else if (action.value === 'mousemiddle') {
				this.actions[action.name] = game.input.activePointer.middleButton;
			} else if (action.value === 'mouseright') {
				this.actions[action.name] = game.input.activePointer.rightButton;
			}
		}
	},

	_setActions: function(array_action_keys) {
        for (action_keys in array_action_keys) {
            this._clearActions(action_keys);
        }
        var action_keys = array_action_keys[0]
		for (action in action_keys) {
			this._setAction(action_keys[action]);
		}
	},

	setGameActions: function() {
        var gameAction_keys = JSON.parse(localStorage.getItem('gameAction_keys'));
        var menuAction_keys = JSON.parse(localStorage.getItem('menuAction_keys'));
		this._setActions([gameAction_keys, menuAction_keys]);
        this.actualActions = "game";
        return this.actions;
	},

	setMenuActions: function() {
        var gameAction_keys = JSON.parse(localStorage.getItem('gameAction_keys'));
        var menuAction_keys = JSON.parse(localStorage.getItem('menuAction_keys'));
		this._setActions([menuAction_keys, gameAction_keys]);
        this.actualActions = "menu";
        return this.actions;
	},
}