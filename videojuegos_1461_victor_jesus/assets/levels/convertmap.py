import re
import json

if __name__ == '__main__':
	import sys
	f_input_n  = sys.argv[1]
	f_output_n = sys.argv[2]

	f_input = open(f_input_n, "r").read()
	tilemap = json.loads(f_input)

	for layer in tilemap["layers"]:
		if layer["name"] == "objects":
			for o in layer["objects"]:
				if "properties" in o:
					o["properties"]["id_tile"] = o["id"]
					o["propertytypes"]["id_tile"] = "int"

	f_output = open(f_output_n, 'w')
	f_output.write(json.dumps(tilemap, sort_keys=True))
