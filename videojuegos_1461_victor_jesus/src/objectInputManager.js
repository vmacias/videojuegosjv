function ObjectInputMaganer() {
    this.input_groups = []
}

ObjectInputMaganer.prototype.add = function(group) {
    this.input_groups.push(group);
}

ObjectInputMaganer.prototype.parseInputs = function(group) {
    for (i in group.children) {
        this.parseInputsSprite(group.children[i]);
    }
}

ObjectInputMaganer.prototype.parseInputsSprite = function(sprite) {
    
    //Para parsear
    var array_inputs = this.stringToArrayNum(sprite.id_inputs);
    
    for (i in this.input_groups) {
        for (j in this.input_groups[i].children) {
            var input_sprite = this.input_groups[i].children[j];
            if (input_sprite.id_tile in array_inputs) {
                sprite.children.push(input_sprite);
                input_sprite.connection = sprite;
            }
        }
    }
}

ObjectInputMaganer.prototype.stringToArrayNum(string) {
    var str_array = string.split(',');
    var int_array = [];
    for (i in str_array) {
        int_array.push(parteInt(str_array[i]));
    }
}