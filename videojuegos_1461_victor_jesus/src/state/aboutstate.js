/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Boot State class, extended from Phaser.State Class
* @class MenuState
* @constructor
*/
function AboutState(game, key) {
    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running this.this.Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into the State Manager. */
};

AboutState.prototype = new MenuState();

AboutState.prototype.constructor = AboutState;

/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method MenuState#create
*/
AboutState.prototype.create = function () {
	// Add a background image
	var background      = this.game.add.image(0, 0, 'background_menu');
	background.height   = this.game.height;
	background.width    = this.game.width;
	background.smoothed = false;
	
	this.stage.disableVisibilityChange = true;
    //var bg = game.add.sprite(0, 0, 'gameover-bg');
    this.addCredit('Music', 'Portal 2 original');
    this.addCredit('Developers', 'Victor Macias Palla & Jesus Serrano Priego');
    this.addCredit('Phaser.io', 'Powered By');
    this.addCredit('for playing', 'Thank you');
    this.createButton("Back", 60, this.game.height*0.9, 200, 50,
	    function () {
            game.sounds.stop_all();
            game.state.start("menu");
	});
    //game.add.tween(bg).to({alpha: 0}, 20000, Phaser.Easing.Cubic.Out, true, 40000);
},

AboutState.prototype.preload = function () {
    //this.optionCount = 1;
    this.creditCount = 0;

  },

AboutState.prototype.addCredit = function(task, author) {
    var authorStyle = { font: '40pt TheMinion', fill: 'white', align: 'center', stroke: 'rgba(0,0,0,0)', strokeThickness: 4};
    var taskStyle = { font: '30pt TheMinion', fill: 'white', align: 'center', stroke: 'rgba(0,0,0,0)', strokeThickness: 4};
    var authorText = game.add.text(this.game.width*0.5, 900, author, authorStyle);
    var taskText = game.add.text(this.game.width*0.5, 950, task, taskStyle);
    authorText.anchor.setTo(0.5);
    authorText.stroke = "rgba(0,0,0,0)";
    authorText.strokeThickness = 4;
    taskText.anchor.setTo(0.5);
    taskText.stroke = "rgba(0,0,0,0)";
    taskText.strokeThickness = 4;
    game.add.tween(authorText).to( { y: -300 }, 8000, Phaser.Easing.Linear.Out, true, this.creditCount * 3000);
    game.add.tween(taskText).to( { y: -250 }, 8000, Phaser.Easing.Linear.Out, true, this.creditCount * 3000);
    this.creditCount ++;
  }

/*MenuState.prototype.createButton = function (string, x, y, w, h, callback) {
    var button = this.game.add.button(x, y, 'menuButton', callback, this, 2, 1, 0);

    button.anchor.setTo(0.1, 0.5);
    button.width  = w;
    button.height = h;

    var txt = this.game.add.text(button.x, button.y, string, {
        font: "14px Slackey",
        align: "left"
    });

    txt.anchor.setTo(0.1, 0.5);
}*/