/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Boot State class, extended from Phaser.State Class
* @class TypeWriterState
* @constructor
*/
function TypeWriterState(game, key, next, text) {
    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running this.this.Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into the State Manager. */
    this.next        = next;
    this.text        = text;
};

TypeWriterState.prototype = new State();

TypeWriterState.prototype.constructor = TypeWriterState;

/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method TypeWriterState#create
*/
TypeWriterState.prototype.create = function () {
	//Sound
	this.game.sounds.add(this.game);

	// Add a background image
    var background      = this.game.add.image(0, 0, 'aperture');
    background.height   = this.game.height;
    background.width    = this.game.width;
    background.smoothed = false;

    var text_exit = this.game.add.text(this.game.width*0.5, this.game.height*0.8, "(Pulse Enter para continuar)", {
        font: "14px Slackey",
        align: "left",
        fill: "#FFFFFF"
    });
    text_exit.anchor.setTo(0.5,0.5);



	//this.add.image(150, 0, "bg");
	var sound = this.game.sounds.game_sounds['typewriter'];
	var game  = this.game;
	var text  = this.text;
    var width = this.game.width-2*300;

	///////////////////////// TYPEWRITER ///////////////////////////////////////////////////////
	var typewriter = new Typewriter();
	typewriter.init(game, {
	  x: 300,
	  y: 80,
	  fontFamily: "shortStack",
	  fontSize: 28,
	  maxWidth: width,
	  sound: sound,
	  text: text
	});
	typewriter.start();

    var typewriterstate = this
    this.game.input.keyboard.onDownCallback = function(e) {
        if (e.keyCode === Phaser.KeyCode.ENTER)
            typewriterstate.nextState();
    }
}

TypeWriterState.prototype.nextState = function () {
    this.game.sounds.stop_all();
    this.game.state.start(this.next);
}


function Typewriter() {
    this.typedText;
    this.timer;
    this.pickedQuote;
    var _that = this;
    var game;
    function init(gameInstance, options) {
        game = gameInstance;
        _that.time = options.time || Phaser.Timer.SECOND / 20;
        _that.sound = options.sound || null;
        _that.writerFn = options.writerFn || null;
        _that.endFn = options.endFn || null;
        _that.times = options.times || 10;
        _that.text = options.text || "";
        _that.x = options.x || 100;
        _that.y = options.y || 100;
        _that.maxWidth = options.maxWidth || 200;
        _that.fontFamily = options.fontFamily || "blackFont";
        _that.fontSize = options.fontSize || 28;
        _that.writerObj = options.writerObj || null;
    }

    function start() {
        enableTypingSpecificMessage(_that.text, _that.x, _that.y);
    }

    function enableTypingSpecificMessage(text, x, y) {

        if (_that.writerObj === null) {
            _that.typedText = game.add.bitmapText(x, y, _that.fontFamily, text, _that.fontSize);
        } else {
            _that.typedText = _that.writerObj;
        }
        _that.typedText.maxWidth = _that.maxWidth;
        _that.currentLetter = 0;
        var length = _that.typedText.children.length;

        for (var i = 0; i < length; i++){
            var letter = _that.typedText.getChildAt(i);
            letter.alpha = 0;
        }

        if (_that.sound !== null) {
            _that.sound.play();
        }

        _that.typedText.x = x;
        _that.typedText.y = y;
        if (_that.endFn !== null) {
            countdown(typeWriter, length, _that.endFn);
        } else {
            countdown(typeWriter, length);
        }
    }

    /**
     * [countDown description]
     * @param  {Function} fn    [description]
     * @param  {[type]}   endFn [description]
     * @return {[type]}         [description]
     */
    function countdown(fn, times, endFn) {
        var _timer = game.time.create(false);
        _timer.start();
        endFn = endFn || function () {
            game.time.events.remove(_timer);
            if (_that.sound !== null) {
                _that.sound.stop();
            }
        };
        _timer.onComplete.add(endFn);
        _timer.repeat(_that.time, times, fn, this);
    }

    function typeWriter(text) {
        if (_that.sound !== null) {
            if (_that.sound.isPlaying === false) {
                _that.sound.play();
            }
        }
        var letter = _that.typedText.getChildAt(_that.currentLetter);
        letter.alpha = 1;
        _that.currentLetter++;
    }

    return {
        init: function (gameInstance, options) {
            init(gameInstance, options);
        },
        start: function () {
            start();
        },
        destroy: function() {
            _that.typedText.destroy();
        },
        hideText: function() {
            _that.typedText.visible = false;
        },
        showText: function() {
            _that.typedText.visible = true;
        },
        moveToTop: function() {
            game.bringToTop(_that.typedText);
        }
    }
}