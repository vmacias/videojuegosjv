function State(game, key) {
    this.game = game;
    this.key = key;
};

State.prototype = new Phaser.State();

State.prototype.constructor = State;

