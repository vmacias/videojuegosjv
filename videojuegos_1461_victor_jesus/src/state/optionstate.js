/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Boot State class, extended from Phaser.State Class
* @class MenuState
* @constructor
*/
function OptionState(game, key) {
    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running this.this.Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into the State Manager. */
};

OptionState.prototype = new MenuState();

OptionState.prototype.constructor = OptionState;

/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method MenuState#create
*/
OptionState.prototype.create = function () {
	// Add a background image
	var background      = this.game.add.image(0, 0, 'background_menu');
	background.height   = this.game.height;
	background.width    = this.game.width;
	background.smoothed = false;
	
	//Add Portal 2D Title
	var titlescreen = this.game.add.text(this.game.width * 0.5, this.game.height * 0.2, 'PORTAL 2D', {
	    font: '70px Slackey',
	    fill: 'blue'
	});
	titlescreen.anchor.setTo(0.5, 0.5);

    
    var txt1 = this.game.add.text(60, this.game.height*0.6, "Music Sound", {
        font: "18px Slackey",
        align: "left"
    });
    txt1.anchor.setTo(0.1, 0.5);
    
    var txt2 = this.game.add.text(351, this.game.height*0.6, Math.round(10*this.game.sounds.music_volume)*10 + "%", {
        font: "16px Slackey",
        align: "left"
    });
    txt2.anchor.setTo(0.1, 0.5);
    
	this.createButton("-", 250, this.game.height*0.6, 100, 40,
	    function () {
            this.game.sounds.change_volume_music(-0.1);
            txt2.setText(Math.round(10*this.game.sounds.music_volume)*10 + "%");
	});
    
	this.createButton("+", 400, this.game.height*0.6, 100, 40,
	    function () {
            this.game.sounds.change_volume_music(0.1);
            txt2.setText(Math.round(10*this.game.sounds.music_volume)*10 + "%");
	});
    
    
    var txt3 = this.game.add.text(60, this.game.height*0.6 + 51, "General Sound", {
        font: "18px Slackey",
        align: "left"
    });
    txt3.anchor.setTo(0.1, 0.5);
    
    var txt4 = this.game.add.text(351, this.game.height*0.6 + 51, Math.round(10*this.game.sounds.game_volume)*10 + "%", {
        font: "16px Slackey",
        align: "left"
    });
    txt4.anchor.setTo(0.1, 0.5);
    
	this.createButton("-", 250, this.game.height*0.6 + 51, 100, 40,
	    function () {
            this.game.sounds.change_volume_game(-0.1);
            txt4.setText(Math.round(10*this.game.sounds.game_volume)*10 + "%");
	});
    
	this.createButton("+", 400, this.game.height*0.6 + 51, 100, 40,
	    function () {
            this.game.sounds.change_volume_game(0.1);
            txt4.setText(Math.round(10*this.game.sounds.game_volume)*10 + "%");
	});
    

    
    var txt5 = this.game.add.text(60, this.game.height*0.6 + 102, "Full Screen", {
        font: "18px Slackey",
        align: "left"
    });

    txt5.anchor.setTo(0.1, 0.5);

	this.createButton("YES", 250, this.game.height*0.6 + 102, 100, 40,
	    function () {
            //Activar pantalla completa
            if (!this.game.scale.isFullScreen)
            {
                this.game.scale.startFullScreen(false);
            }
	});
    
    this.createButton("NO", 400, this.game.height*0.6 + 102, 100, 40,
	    function () {
            //Desactivar el sonido
            if (this.game.scale.isFullScreen)
            {
                this.game.scale.stopFullScreen();
            }
	});

	this.createButton("Control", 75, this.game.height*0.6 + 153, 300, 40, 
	    function () {
	        this.game.state.start('control');
	});
    
    this.createButton("Done", 75, this.game.height*0.6 + 204, 300, 40, 
	    function () {
	    	this.game.sounds.stop_all();
	        this.game.state.start('menu');
	});

	// fullscreen not working
	//this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.RESIZE;
}

/*MenuState.prototype.createButton = function (string, x, y, w, h, callback) {
    var button = this.game.add.button(x, y, 'menuButton', callback, this, 2, 1, 0);

    button.anchor.setTo(0.1, 0.5);
    button.width  = w;
    button.height = h;

    var txt = this.game.add.text(button.x, button.y, string, {
        font: "14px Slackey",
        align: "left"
    });

    txt.anchor.setTo(0.1, 0.5);
}*/