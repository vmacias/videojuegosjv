/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Boot State class, extended from Phaser.State Class
* @class LoadState
* @constructor
*/
function LoadState(game, key) {
    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running this.Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into the State Manager. */
};

LoadState.prototype = new State();

LoadState.prototype.constructor = LoadState;

/**
* preload is called first. Normally you'd use this to load your game assets (or those needed for the current State)
* You shouldn't create any objects in this method that require assets that you're also loading in this method, as
* they won't yet be available.
*
* @method LoadState#preload
*/
LoadState.prototype.preload = function () {

	this.game.canvas.oncontextmenu = function (e) { e.preventDefault(); }


	// Add a 'loading...' label on the screen
	var loadingLabel = this.game.add.text(this.game.world.centerX, 150, 'loading...', {
	    font: '30px Arial',
	    fill: '#ffffff'
	});
	loadingLabel.anchor.setTo(0.5, 0.5);
	
	// Display the progress bar
	var progressBar = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'progressBar');
	progressBar.anchor.setTo(0.5, 0.5);
	this.game.load.setPreloadSprite(progressBar);

	// Font
	this.game.load.bitmapFont('shortStack','assets/fonts/shortStack.png', 'assets/fonts/shortStack.fnt');


	// Load all our assets

	///////////////////////
	// Sound
	this.game.sounds.load(this.game);

	///////////////////////
	// Player
	this.game.load.spritesheet('player', 'assets/sprites/spriteplayer.png', 155, 350);
	this.game.load.image('head', 'assets/sprites/head.png');
	this.game.load.image('body', 'assets/sprites/body.png');
	this.game.load.image('arm', 'assets/sprites/arm.png');
	this.game.load.image('empty_arm', 'assets/sprites/empty_arm.png');


	///////////////////////
	// Objects

	// Weapons and bullets
	this.game.load.spritesheet('bullet', 'assets/sprites/bullet.png',24,10);

	// Portals
	this.game.load.spritesheet('portal', 'assets/sprites/portal.png', 32, 150);

	///////////////////////
	// Tileset / Tilemap
	this.game.load.image('background',      'assets/tilesets/background.png');
	this.game.load.image('objects',         'assets/tilesets/objects.png');
	this.game.load.image('walls64',         'assets/tilesets/walls64.png');
	this.game.load.image('collision_walls', 'assets/tilesets/collision_walls.png');
	this.game.load.tilemap('mapa1',         'assets/levels/mapa1.json', null, Phaser.Tilemap.TILED_JSON);
	this.game.load.tilemap('mapa2',         'assets/levels/mapa2.json', null, Phaser.Tilemap.TILED_JSON);
	this.game.load.tilemap('mapa3',         'assets/levels/mapa3.json', null, Phaser.Tilemap.TILED_JSON);
	this.game.load.tilemap('mapa4',         'assets/levels/mapa4.json', null, Phaser.Tilemap.TILED_JSON);
	this.game.load.tilemap('mapa5',         'assets/levels/mapa5.json', null, Phaser.Tilemap.TILED_JSON);
	this.game.load.tilemap('mapa6',         'assets/levels/mapa6.json', null, Phaser.Tilemap.TILED_JSON);	this.game.load.tilemap('mapa7',         'assets/levels/mapa7.json', null, Phaser.Tilemap.TILED_JSON);	this.game.load.tilemap('mapa8',         'assets/levels/mapa8.json', null, Phaser.Tilemap.TILED_JSON);	this.game.load.tilemap('mapa9',         'assets/levels/mapa9.json', null, Phaser.Tilemap.TILED_JSON);	this.game.load.tilemap('mapa10',         'assets/levels/mapa10.json', null, Phaser.Tilemap.TILED_JSON);


	///////////////////////
	// Object Tilemap

	PedestalButton.load(this.game)
	Button.load(this.game)
	Door.load(this.game)
	Dropper.load(this.game)
    
	///////////////////////
    // Other objects
    Cube.load(this.game)

	///////////////////////
	// Particles
	//this.game.load.image('pixel', 'assets/pixel.png');
	
	
	
	///////////////////////
	// Menu assets
	this.game.load.image('background_menu', 'assets/images/background_menu.png');
	this.game.load.spritesheet('menuButton', 'assets/images/menuButton.png', 150, 30);
	
	///////////////////////
	// Typewriter assets
	this.game.load.image('aperture', 'assets/images/aperture.png');

	///////////////////////
	// Sound
	
	// Load default actions in localstorage if first game
	if (!localStorage.getItem('menuAction_keys')) {
	    localStorage.setItem('menuAction_keys', JSON.stringify(Action.DefaultMenuActions()));
	}

	if (!localStorage.getItem('gameAction_keys')) {
	    localStorage.setItem('gameAction_keys', JSON.stringify(Action.DefaultGameActions()));
	}
}


/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method LoadState#create
*/
LoadState.prototype.create = function () {
    // Go to the menu state
    this.game.state.start('menu');
}