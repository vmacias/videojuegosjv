/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Boot State class, extended from Phaser.State Class
* @class BootState
* @constructor
*/
function BootState(game, key) {

    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into the State Manager. */
};

BootState.prototype = new State();

BootState.prototype.constructor = BootState;

/**
* preload is called first. Normally you'd use this to load your game assets (or those needed for the current State)
* You shouldn't create any objects in this method that require assets that you're also loading in this method, as
* they won't yet be available.
*
* @method BootState#preload
*/
BootState.prototype.preload = function () {
    this.game.load.image('progressBar', 'assets/progressBar.png');
}

/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method BootState#create
*/
BootState.prototype.create = function () {
    // Set some game settings
    this.game.stage.backgroundColor = '#3498db';
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    // Start the load state
    this.game.state.start('load');
}