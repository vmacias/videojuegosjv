/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Level State class, extended from Phaser.State Class
* @class LevelState
* @constructor
*/
function LevelState(game, key, level, next, tilemap_key, bulletLeftColor, bulletRightColor) {
    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into the State Manager. */
    this.level       = level;
    this.next        = next;
    this.tilemap_key = tilemap_key; /** @property {string} tilemap_key - Tilemap identifier of the state */
    this.portalTimer = 0;
    this.flag_buttons = 0;


    this.sprite_through_portal = null;
    this.overlap_portal_right = false;
    this.overlap_portal_left  = false;

    if (bulletLeftColor === undefined) {
        this.bulletLeftColor = 0x0073e6;
    } else {
        this.bulletLeftColor = bulletLeftColor;
    }

    if (bulletRightColor === undefined) {
        this.bulletRightColor = 0xff9933;
    } else {
        this.bulletRightColor = bulletRightColor;
    }

};

LevelState.prototype = new State();

LevelState.prototype.constructor = LevelState;

/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method LevelState#create
*/
LevelState.prototype.create = function () {
    this.last_action = this.game.time.now;

    // Sounds
    this.game.sounds.add(this.game);
    this.game.sounds.play_background(this.level);

    /////////////////
    // Game action
    var action = new Action();
    this.actions = action.setGameActions();
    this.game.actions = this.actions;

    /////////////////
    // Load Map
    this.map = this.game.add.tilemap(this.tilemap_key);
    this.map.addTilesetImage('objects');
    this.map.addTilesetImage('walls64');
    this.map.addTilesetImage('collision_walls');
    this.map.addTilesetImage('background');

  

    /////////////////
    // Elements in canvas (order is important!)

    // first layers
    this.layer_background = this.map.createLayer('background');
    this.layer_updown     = this.map.createLayer('updown');
    this.layer_rightleft  = this.map.createLayer('rightleft');
    
    /////////////////
    // Load base groups and managers
    this.tilemapGroup = this.game.add.group();
    this.cubeGroup    = this.game.add.group();
    this.inputManager = new ObjectInputManager();

    // input items
    this.pedestalButton = new PedestalButton(this.game, this.tilemapGroup, this.map, 'objects', this.inputManager);
    this.button         = new Button(        this.game, this.tilemapGroup, this.map, 'objects', this.inputManager);

    // output objects
    this.dropper        = new Dropper(       this.game, this.tilemapGroup, this.map, 'objects', this.inputManager, this.cubeGroup);
    this.levelEnd       = new LevelEnd(      this.game, this.tilemapGroup, this.map, 'objects', this.inputManager);

    // start
    this.levelStart     = new LevelStart(    this.game, this.tilemapGroup, this.map, 'objects');


    // Player
    this.player         = new Player(this.game, this.layer_collision, this.actions, this.levelStart, this);

    // Player Weapons
    this.weapons = new Weapons(this.game, this.player.empty_arm, this.actions, this.bulletLeftColor, this.bulletRightColor);

    // Portals
    this.portals = new Portals(this.game, this.bulletLeftColor, this.bulletRightColor);
    this.weapons.create();
    this.portals.create();

    // Portalable group
    this.cubeGroup.add(this.player)
    this.portalableGroup = this.cubeGroup;

    // Finally: layer collision
    this.layer_collision  = this.map.createLayer('collision');
    this.layer_collision.resizeWorld();
    this.map.setCollisionBetween(1, 200, true, 'collision'); // ajustar el numerico

    // Pause button
    this.game.input.keyboard.onDownCallback = this.update_pause;
}

/**
* It is called during the core game loop AFTER debug, physics, plugins and the Stage have had their preUpdate methods called.
* It is called BEFORE Stage, Tweens, Sounds, Input, Physics, Particles and Plugins have had their postUpdate methods called.
*
* @method LevelState#update
*/
LevelState.prototype.update = function() {
    this.game.physics.arcade.overlap(this.player, this.levelEnd, this.levelEnd.callbackSpriteGroup);

    this.update_actions();

    this.update_button();

    this.update_collision();

    this.update_fire();

    this.update_portal();

    this.player.update();

    this.weapons.update();

    this.update_pause();
}

LevelState.prototype.nextState = function (levelState) {
    this.game.sounds.stop_all();
    levelState.game.camera.reset();
    levelState.state.start(levelState.next);
}




/*
var player       = this.player;
var game         = this.game;
var weapons      = this.weapons;
var weapon_left  = weapons.weapon_left;
var weapon_right = weapons.weapon_right;
var portals      = this.portals;


var layer_collision = this.layer_collision;
var portalTimer = this.portalTimer;

var overlap_portal_left = this.overlap_portal_left;
var overlap_portal_right = this.overlap_portal_right;
*/

LevelState.prototype.update_actions = function () {
    if (this.actions.action.isDown && this.game.time.now - this.last_action > 100) {
        this.last_action = this.game.time.now;
        
        if (this.player.dropItem()) {}
        
        // BOTON DE PEDESTAL
        else if (this.game.physics.arcade.overlap(this.player, this.pedestalButton, this.pedestalButton.callbackSpriteGroup)){}
        
        // COGER CUBO
        else if(this.game.physics.arcade.overlap(this.player, this.cubeGroup.children, this.player.catchItem)){}
    }
}

LevelState.prototype.update_button = function () {
    this.game.physics.arcade.overlap(this.cubeGroup, this.button, this.button.callbackSpriteGroup);
    this.game.physics.arcade.overlap(this.player,    this.button, this.button.callbackSpriteGroup);

    this.button.update();
}

LevelState.prototype.update_collision = function () {
    this.game.physics.arcade.collide(this.player,    this.layer_collision);
    this.game.physics.arcade.collide(this.cubeGroup, this.layer_collision);    
}

LevelState.prototype.update_fire = function () {
    var weapons      = this.weapons;
    var weapon_left  = weapons.weapon_left;
    var weapon_right = weapons.weapon_right;
    var portals      = this.portals;
    var game         = this.game;

    // left portal
    this.game.physics.arcade.collide(this.layer_collision, weapon_left.bullets, function() {
        var body = weapons.weapon_left.bullets.children[0].body;

        portals.setLeftPortal(body.x, body.y);
        portals.rotateLeftPortal(body.blocked);
        weapon_left.killAll();
    }, null, game);

    // right portal
    this.game.physics.arcade.collide(this.layer_collision, weapon_right.bullets, function() {
        var body = weapons.weapon_right.bullets.children[0].body;
        portals.setRightPortal(body.x, body.y);
        portals.rotateRightPortal(body.blocked);
        weapon_right.killAll();
    }, null, game);
}

LevelState.prototype.update_portal = function () {
    var game                  = this.game;
    var overlap_portal_left   = this.overlap_portal_left;
    var overlap_portal_right  = this.overlap_portal_right;
    var sprite_through_portal = this.sprite_through_portal;
    var portalTimer           = this.portalTimer;

    if (this.game.time.now > this.portalTimer) {
        this.game.physics.arcade.overlap(this.portals.portal_left, this.portalableGroup, function(portal, child) {
            sprite_through_portal = child;
            overlap_portal_left = true;
            portalTimer = game.time.now + 200;
        }, null, game);

        this.game.physics.arcade.overlap(this.portals.portal_right, this.portalableGroup, function(portal, child) {
            sprite_through_portal = child;
            overlap_portal_right = true;
            portalTimer = game.time.now + 200;
        }, null, game);
        this.sprite_through_portal = sprite_through_portal;
        this.overlap_portal_left  = overlap_portal_left;
        this.overlap_portal_right = overlap_portal_right;
        this.portalTimer          = portalTimer;
    }

    // ATRAVESAR PORTALES
    if (this.overlap_portal_right) {
        this.portals.sprite_through_right_portal(this.sprite_through_portal);
        this.overlap_portal_right = false;
    } else if (this.overlap_portal_left) {
        this.portals.sprite_through_left_portal(this.sprite_through_portal);
        this.overlap_portal_left = false;
    }
}


var pauselabel;

LevelState.prototype.update_pause = function (event) {
    if (event === undefined)
        return

    if (event.keyCode === this.game.actions.pause.keyCode) {
        if (this.game.paused) {
            this.game.paused = false;
            pauselabel.destroy();
        } else {
            this.game.paused = true;

            pauselabel = this.game.add.text(this.game.width*0.5, this.game.height*0.5, "Pause", {
                font: "24px Slackey",
                align: "left",
                backgroundColor: "#909090"
            });


        }
    }
}















// DEBUG
LevelState.prototype.render = function () {
    //this.game.debug.body(this.portals.portal_left);
    //this.game.debug.body(this.portals.portal_right);
    //this.game.debug.body(this.player);
    //this.game.debug.body(this.pedestalButton.children[0]);
    //this.game.debug.body(this.cubeGroup);
    //this.game.debug.bodyInfo(this.player, 32, 32)
    //this.cubeGroup.forEachAlive(this.renderGroup, this);
}

LevelState.prototype.renderGroup = function(member) {
    game.debug.body(member);
}
