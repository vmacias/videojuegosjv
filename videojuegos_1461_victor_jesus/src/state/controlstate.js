/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Boot State class, extended from Phaser.State Class
* @class MenuState
* @constructor
*/
function ControlState(game, key) {
    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running this.this.Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into
    the State Manager. */
    this.action = new Action();
    this.lastclicked = null;
    //this.keyboard = new Keyboard();
};

ControlState.prototype = new MenuState();

ControlState.prototype.constructor = ControlState;

/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method MenuState#create
*/
ControlState.prototype.create = function () {
    this.incontrol = true;

	// Add a background image
	var background      = this.game.add.image(0, 0, 'background_menu');
	background.height   = this.game.height;    
	background.width    = this.game.width;
	background.smoothed = false;
    
    var listActions = Action.getGameActions();
	
	//Add Portal 2D Title
	var titlescreen = this.game.add.text(this.game.width * 0.5, this.game.height * 0.2, 'PORTAL 2D', {
	    font: '70px Slackey',
	    fill: 'blue'
	});
	titlescreen.anchor.setTo(0.5, 0.5);



    i = 0;
    for (key in listActions) {
        if (listActions[key].type === "keyboard") {
            var txt = this.game.add.text(60, this.game.height*0.6 + i*51, "Player ".concat(key), {
                font: "18px Slackey",
                align: "left"
            });
            txt.anchor.setTo(0.1, 0.5);

            var string = listActions[key].display;

            this.createButton(string, 300, this.game.height*0.6 + i*51, 300, 40,
                function (button) {
                    if (this.lastclicked !== button) {
                        this.lastclicked = button;
                        button.setFrames(2)
                    } else {
                        this.lastclicked = null;
                        button.setFrames(1)
                    }
                }, key
            );

            i++;            
        }
    }
    
    this.createButton("Done", 60, this.game.height*0.6 + 255, 150, 40, 
	    function () {
            this.action.setGameActions();
            this.incontrol = false;
            this.game.state.start('options');

	});

    var controlstate = this;

    this.game.input.keyboard.onDownCallback = function(e) {
        if (controlstate.incontrol) {
            if (controlstate.lastclicked !== null)
                controlstate.action.changeGameAction(controlstate.lastclicked.id, "keyboard", e.keyCode, e.key);
                controlstate.lastclicked.txt.setText(e.key)            
        }
    }
}
