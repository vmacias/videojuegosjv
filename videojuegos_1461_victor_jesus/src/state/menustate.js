/**
* @author Jesus Serrano <jesus.serranop@estudiante.uam.es> Victor Macias <victor.macias@estudiante.uam.es>
*/

/**
* This is a base Boot State class, extended from Phaser.State Class
* @class MenuState
* @constructor
*/
function MenuState(game, key) {
    this.game        = game;        /** @property {Phaser.Game} game   - This is a reference to the currently running this.this.Game. */
    this.key         = key;         /** @property {string} key         - The string based identifier given to the State when added into the State Manager. */
};

MenuState.prototype = new State();

MenuState.prototype.constructor = MenuState;

/**
* Create is called once preload has completed, this includes the loading of any assets from the Loader.
*
* @method MenuState#create
*/
MenuState.prototype.create = function () {
	//Sound
	this.game.sounds.add(this.game);
	this.game.sounds.play_menu_sound();

	// Add a background image
	var background      = this.game.add.image(0, 0, 'background_menu');
	background.height   = this.game.height;
	background.width    = this.game.width;
	background.smoothed = false;
	
	//Add Portal 2D Title
	var titlescreen = this.game.add.text(this.game.width * 0.5, this.game.height * 0.2, 'PORTAL 2D', {
	    font: '70px Slackey',
	    fill: 'blue',
        //backgroundColor: 'grey',
	});
	titlescreen.anchor.setTo(0.5, 0.5);

	this.createButton("Play", 60, this.game.height*0.7, 300, 50,
	    function () {
	    	this.game.sounds.stop_all();
	        this.game.state.start('intro');
	});

	this.createButton("Options", 60, this.game.height*0.7 + 61, 300, 50,
	    function () {
	        this.game.state.start('options');
	});

	this.createButton("About", 60, this.game.height*0.7 + 122, 300, 50, 
	    function () {
	        this.game.state.start('about');
	});

	// fullscreen not working
	//this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.RESIZE;
}

MenuState.prototype.createButton = function (string, x, y, w, h, callback, key) {
    var button = this.game.add.button(x, y, 'menuButton', callback, this, 2, 1, 0);
	if (key !== null)
    	button.id = key

    button.anchor.setTo(0.1, 0.5);
    button.width  = w;
    button.height = h;

    var txt = this.game.add.text(button.x, button.y, string, {
        font: "17px Slackey",
        align: "left"
    });

    txt.anchor.setTo(0.1, 0.5);
    
    button.txt = txt;
    return txt;
}