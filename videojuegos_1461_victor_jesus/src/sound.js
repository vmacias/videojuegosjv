function Sound() {
	this.game_sounds = {};
	this.music_sounds   = {};

	this.music_sounds['background_sound_01']      = null;
	this.music_sounds['background_sound_02']      = null;
	this.music_sounds['background_sound_03']      = null;
	this.music_sounds['background_sound_04']      = null;
	this.music_sounds['background_sound_05']      = null;

	this.game_sounds['button_close']           = null;
	this.game_sounds['button_open']            = null;

	this.game_sounds['door_close']             = null;
	this.game_sounds['door_open']              = null;

	this.game_sounds['dropper_open']           = null;
	this.game_sounds['dropper_close']          = null;

	this.music_sounds['menu_sound']            = null;

	this.game_sounds['pedestal_button_open']   = null;
	this.game_sounds['pedestal_button_close']  = null;
	this.game_sounds['pedestal_button_tictoc'] = null;

	this.game_sounds['player_step']            = null;
	/*this.game_sounds['player_jump']            = null;*/

	this.game_sounds['portal_enter']           = null;
	this.game_sounds['portal_open']            = null;

	this.game_sounds['weapon_fire_left']       = null;
	this.game_sounds['weapon_fire_right']      = null;

	this.game_sounds['typewriter']             = null;

	this.music_volume = 1;
	this.game_volume  = 1;

}

Sound.prototype.load = function (game) {
	for (key in this.game_sounds) {
		game.load.audio(key, 'assets/sounds/'.concat(key).concat(".mp3"));
	}

	for (key in this.music_sounds) {
		game.load.audio(key, 'assets/sounds/'.concat(key).concat(".mp3"));
	}


}

Sound.prototype.add = function (game) {
	for (key in this.game_sounds) {
		this.game_sounds[key] = game.add.audio(key);
		this.game_sounds[key].volume = this.game_volume;
	}

	for (key in this.music_sounds) {
		this.music_sounds[key] = game.add.audio(key);
		this.music_sounds[key].volume = this.music_volume;
	}


	this.music_sounds['background_sound_01'].loop = true;
	this.music_sounds['background_sound_02'].loop = true;
	this.music_sounds['background_sound_03'].loop = true;
	this.music_sounds['background_sound_04'].loop = true;
	this.music_sounds['background_sound_05'].loop = true;

	this.music_sounds['menu_sound'].loop = true;

	this.game_sounds['pedestal_button_tictoc'].loop = true;
}

Sound.prototype.play_background = function (n) {
	this.music_sounds['background_sound_0'.concat(String(n))].play();
}

Sound.prototype.stop_background = function (n) {
	this.music_sounds['background_sound_0'.concat(String(n))].stop();
}

Sound.prototype.play_menu_sound = function () {
	if (!this.music_sounds['menu_sound'].isPlaying)
		this.music_sounds['menu_sound'].play();
}

Sound.prototype.stop_menu_sound = function () {
	this.music_sounds['menu_sound'].stop();
}

Sound.prototype.play_player_step = function () {
	if (!this.game_sounds['player_step'].isPlaying)
		this.game_sounds['player_step'].play();
}

Sound.prototype.play_game_sound = function (str) {
	this.game_sounds[str].play();
}

Sound.prototype.stop_game_sound = function (str) {
	this.game_sounds[str].stop();
}

Sound.prototype.stop_all = function () {
	for (key in this.game_sounds) {
		this.game_sounds[key].stop();
	}

	for (key in this.music_sounds) {
		this.music_sounds[key].stop();
	}
}

Sound.prototype.change_volume_music = function (volume) {
	this.music_volume += volume;
	this.music_volume = Math.max(0, this.music_volume);
	this.music_volume = Math.min(1, this.music_volume);
	
	for (key in this.music_sounds) {
		this.music_sounds[key].volume = this.music_volume;
	}
}

Sound.prototype.change_volume_game = function (volume) {
	this.game_volume += volume;
	this.game_volume = Math.max(0, this.game_volume);
	this.game_volume = Math.min(1, this.game_volume);
	
	for (key in this.game_sounds) {
		this.game_sounds[key].volume = this.game_volume;
	}
}
