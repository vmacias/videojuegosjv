function ObjectPortalable(game, dropper, x, y) {
	this.input_groups = [];
};

ObjectPortalable.prototype.add = function(group) {
    this.input_groups.push(group);
}

ObjectPortalable.prototype.parseInputs = function(group) {
    for (i in group.children) {
        this.parseInputsSprite(group.children[i]);
    }
}

// funciones