function Cube(game, dropper, x, y) {
	Phaser.Sprite.call(this, game, x, y, 'cube');

	// Dropper associated
	this.dropper = dropper;

	// Player who is taking the cube
	this.playertaken = null;

	// Physics
	this.scale.setTo(0.1, 0.1);
	this.anchor.setTo(0.5,0.5);
	this.pivot.x = -500;
	game.physics.arcade.enable(this);
	this.body.drag.x=1000;
	this.body.gravity.y = 800;
	this.body.setSize(850,850,-225,-225);
	this.body.maxVelocity.y = 1000;
	this.body.collideWorldBounds = true;

	// Add sprite to game
	game.add.existing(this);
};

Cube.prototype = Object.create(Phaser.Sprite.prototype);

Cube.prototype.constructor = Cube;

//funciones

Cube.prototype.callbackSpriteSprite = function(player, cube) {
    if (cube.playertaken !== null) {
    	cube.playertaken = null;
    	player.dropItem();
    } else {
    	cube.playertaken = player;
    	player.catchItem(cube);
    }
}

Cube.prototype.destroy = function() {
	//añadir reset jugador
	if (this.playertaken !== null) {
		this.playertaken.dropItem();
	}

	Phaser.Sprite.prototype.destroy.call(this);

	//añadir animacion de destruccion
}

// funciones de clase
Cube.load = function (game) {
	game.load.image('cube', 'assets/sprites/cube.png');
}