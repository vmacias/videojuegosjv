function LevelStart(game, parent, tilemap, layername) {
	Door.call(this, game, parent, tilemap, layername, 4, 'door');

	this.start_x = this.children[0].x;
	this.start_y = this.children[0].y;
};

LevelStart.prototype = Object.create(Door.prototype);

LevelStart.constructor = LevelStart;

// funciones

// Overrides
LevelStart.prototype.animationIni = function (player) {
	var door = this.children[0];
	this.open(this.children[0]);

	door.game.time.events.add(this.timeanimation, door.parent.animationMiddle, this, player, door);
}

LevelStart.prototype.animationMiddle = function (player, door) {
	player.x = door.x;
	player.y = door.y-player.anchor.y*player.height-50;
	player.visible = true;
	this.close(this.children[0]);
}
