function Door(game, parent, tilemap, layername, idtile, key) {
	this.animation_keys = {
		"open": {
			"name"      : "open",
			"frames"    : [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
			"frameRate" : 40,
			"loop"      : false
		},
		"close": {
			"name"      : "close",
			"frames"    : [15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0],
			"frameRate" : 40,
			"loop"      : false
		}
	};

	this.properties = {
		"scale":2,
		"anchor": {x:0,y:0.8}
	}

	this.timeanimation = 500;

	ObjectTilemap.call(this, game, parent, tilemap, layername, idtile, key);
};

Door.prototype = Object.create(ObjectTilemap.prototype);

Door.constructor = Door;

// funciones

// Propias
Door.prototype.open = function (child) {
	child.animations.play("open");
	child.game.sounds.play_game_sound('door_open');
	child.isopen = true;
}

Door.prototype.close = function (child) {
	child.animations.play("close");
	child.game.sounds.play_game_sound('door_close');
	child.isopen = false;
}

// funciones de clase
Door.load = function (game) {
	game.load.spritesheet('door', 'assets/sprites/door.png', 62, 52);
}