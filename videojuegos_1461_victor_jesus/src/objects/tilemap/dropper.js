function Dropper(game, parent, tilemap, layername, inputManager, cubeGroup) {
	this.animation_keys = {
		"open": {
			"name"      : "open",
			"frames"    : [0,1,2,3,4,5,6,7],
			"frameRate" : 40,
			"loop"      : false
		},
		"close": {
			"name"      : "close",
			"frames"    : [7,6,5,4,3,2,1,0],
			"frameRate" : 40,
			"loop"      : false
		}
	};

	this.properties = {
		"scale":1,
		"anchor": {x:0,y:-0.4}
	};

	this.timeanimation = 1000;

	this.cubeGroup = cubeGroup;

	ObjectTilemap.call(this, game, parent, tilemap, layername, 6, 'dropper');

	inputManager.parseInputs(this);
};

// no se si esto es asi
Dropper.prototype = Object.create(ObjectTilemap.prototype);

Dropper.constructor = Dropper;

// funciones

Dropper.prototype.updateState = function (child) {
	for (i in child.children) {
		if (!child.children[i].activated) {
			if (child.isopen) {
				this.close(child);
			}
			return;
		}
	}

	if (!child.isopen) {
		this.open(child);
	}
}

// Propias
Dropper.prototype.open = function (child) {
    child.animations.play("open");
    child.game.sounds.play_game_sound('dropper_open');
    child.isopen = true;

    //CREAR CUBOO
    if (child.item) {
    	child.parent.cubeGroup.removeChild(child.item);
    	child.item.destroy();
    	child.item = null;
    }

    child.item = new Cube(child.game, child, child.x-3, child.y+85);
    child.parent.cubeGroup.add(child.item);
    
}

Dropper.prototype.close = function (child) {
	child.animations.play("close");
	child.game.sounds.play_game_sound('dropper_close');
	child.isopen = false;
}

// funciones de clase
Dropper.load = function (game) {
	game.load.spritesheet('dropper', 'assets/sprites/dropper.png', 96, 99);
}