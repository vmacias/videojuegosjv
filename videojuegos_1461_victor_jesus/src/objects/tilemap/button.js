function Button(game, parent, tilemap, layername, inputManager) {
	this.game = game;
	this.animation_keys = {
		"turnon": {
			"name"      : "turnon",
			"frames"    : [0,1,2,3],
			"frameRate" : 10,
			"loop"      : false
		},
		"turnoff": {
			"name"      : "turnoff",
			"frames"    : [3,2,1,0],
			"frameRate" : 10,
			"loop"      : false
		}
	};
    
    this.properties = {
		"scale":0.5,
		"anchor": {x:-0.5,y:-0.25}
	}

	ObjectTilemap.call(this, game, parent, tilemap, layername, 2, 'button');

	this.actualOverlaping = [];

	inputManager.add(this);
};

Button.prototype = Object.create(ObjectTilemap.prototype);

Button.prototype.constructor = Button;

// funciones

// Overrides
Button.prototype.callbackSpriteGroup = function (sprite, buttonChild) {
	if (!buttonChild.activated) {
		var parent = buttonChild.parent;
		parent.turnon(buttonChild);
		parent.addOverlap(sprite, buttonChild);
		//buttonChild.game.time.events.add(Phaser.Timer.SECOND * buttonChild.segundos, parent.turnoff, this, buttonChild);
	}
}

Button.prototype.turnoff = function (buttonChild) {
    if (buttonChild.activated) {
        buttonChild.animations.play("turnoff");
        buttonChild.game.sounds.play_game_sound('button_close');
        buttonChild.activated = false;
        buttonChild.connection.parent.updateState(buttonChild.connection);
    }
}

// Propias
Button.prototype.turnon = function (buttonChild) {
	buttonChild.animations.play("turnon");
	buttonChild.game.sounds.play_game_sound('button_open');
	buttonChild.activated = true;
	buttonChild.connection.parent.updateState(buttonChild.connection);
}

Button.prototype.addOverlap = function (sprite, buttonChild) {
	this.actualOverlaping.push([sprite, buttonChild]);
}

Button.prototype.update = function () {
	for (i in this.actualOverlaping) {
		var sprite      = this.actualOverlaping[i][0];
		var buttonChild = this.actualOverlaping[i][1]; 
		if (!this.game.physics.arcade.overlap(sprite, buttonChild)) {
			buttonChild.parent.turnoff(buttonChild);
			this.actualOverlaping.splice(i,1);
			return;
		}
	}
}

// funciones de clase
Button.load = function (game) {
	game.load.spritesheet('button', 'assets/sprites/button.png', 97, 31);
}