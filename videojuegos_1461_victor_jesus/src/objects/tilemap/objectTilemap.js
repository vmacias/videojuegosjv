function ObjectTilemap(game, parent, tilemap, layername, idtile, key) {
	Phaser.Group.call(this, game, parent);

	this.tilemap    = tilemap;
	this.layername  = layername;
	this.idtile     = idtile;
	this.key        = key;

	this.enableBody = true;
	this.createFromTilemap();
	this.configure();
	this.addAllAnimation();
};

ObjectTilemap.prototype = Object.create(Phaser.Group.prototype);

ObjectTilemap.constructor = ObjectTilemap;

// funciones
ObjectTilemap.prototype.createFromTilemap = function () {
	this.tilemap.createFromObjects(this.layername, this.idtile, this.key, 0, true, false, this);
}

ObjectTilemap.prototype.configure = function () {
	var properties = this.properties;
	this.callAll('anchor.setTo', 'anchor', properties.anchor.x, properties.anchor.y);
	this.callAll('scale.setTo', 'scale', properties.scale, properties.scale);
}

ObjectTilemap.prototype.addAllAnimation = function () {
	for (animation in this.animation_keys) {
		this.addAnimation(this.animation_keys[animation]);
	}
}

ObjectTilemap.prototype.addAnimation = function (animation) {
	this.callAll('animations.add', 'animations', animation.name, animation.frames, animation.frameRate, animation.loop);
}

ObjectTilemap.prototype.playAnimation = function (name) {
	this.callAll('animations.play', 'animations', name);
}

ObjectTilemap.prototype.callbackSpriteGroup = function (sprite, child) {
	return;
}
