function ObjectInputManager() {
    this.input_groups = [];
}

ObjectInputManager.prototype.add = function(group) {
    this.input_groups.push(group);
}

ObjectInputManager.prototype.parseInputs = function(group) {
    for (i in group.children) {
        this.parseInputsSprite(group.children[i]);
    }
}

ObjectInputManager.prototype.parseInputsSprite = function(sprite) {
    
    //Para parsear
    var array_inputs = this.stringToArrayNum(sprite.id_inputs);
    
    for (i in this.input_groups) {
        for (j in this.input_groups[i].children) {
            var input_sprite = this.input_groups[i].children[j];
            if (-1 !== array_inputs.indexOf(input_sprite.id_tile)) {
                input_sprite.connection = sprite;
                sprite.children.push(input_sprite);
            }
        }
    }
}

ObjectInputManager.prototype.stringToArrayNum = function(string) {
    var str_array = string.split(',');
    var int_array = [];
    for (i in str_array) {
        int_array.push(parseInt(str_array[i]));
    }

    return int_array;
}