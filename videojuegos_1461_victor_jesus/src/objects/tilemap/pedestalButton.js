function PedestalButton(game, parent, tilemap, layername, inputManager) {
	this.animation_keys = {
		"turnon": {
			"name"      : "turnon",
			"frames"    : [0,1,2,3],
			"frameRate" : 10,
			"loop"      : false
		},
		"turnoff": {
			"name"      : "turnoff",
			"frames"    : [3,2,1,0],
			"frameRate" : 10,
			"loop"      : false
		}
	};

	this.properties = {
		"scale":0.5,
		"anchor": {x:-0.5,y:-1}
	}

	ObjectTilemap.call(this, game, parent, tilemap, layername, 1, 'pedestalbutton');

	inputManager.add(this);
};

PedestalButton.prototype = Object.create(ObjectTilemap.prototype);

PedestalButton.constructor = PedestalButton;

// funciones

// Overrides
PedestalButton.prototype.callbackSpriteGroup = function (sprite, child) {
	if (!child.activated) {
		var parent = child.parent;
		parent.turnon(child);
		child.game.time.events.add(Phaser.Timer.SECOND * child.segundos, parent.turnoff, this, child);
	}
}

// Propias
PedestalButton.prototype.turnon = function (child) {
	child.animations.play("turnon");
	child.game.sounds.play_game_sound('pedestal_button_open');
	child.game.sounds.play_game_sound('pedestal_button_tictoc');

	child.activated = true;
	child.connection.parent.updateState(child.connection);
}

PedestalButton.prototype.turnoff = function (child) {
	child.animations.play("turnoff");
	child.game.sounds.play_game_sound('pedestal_button_close');
	child.game.sounds.stop_game_sound('pedestal_button_tictoc');
	child.activated = false;
	child.connection.parent.updateState(child.connection);
}

// funciones de clase
PedestalButton.load = function (game) {
	game.load.spritesheet('pedestalbutton', 'assets/sprites/pedestalbutton.png', 64, 194);
}