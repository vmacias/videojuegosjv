function LevelEnd(game, parent, tilemap, layername, inputManager) {
	Door.call(this, game, parent, tilemap, layername, 5, 'door');

	this.close(this.children[0]);

	inputManager.parseInputs(this);
};

LevelEnd.prototype = Object.create(Door.prototype);

LevelEnd.constructor = LevelEnd;

// funciones

LevelEnd.prototype.callbackSpriteGroup = function (player, child) {
	if (child.isopen) {

		player.context.nextState(player.context);
		//player.game.time.events.add(0, player.context.nextState, this, player.context);
		player.visible = false;
		child.parent.close(child);

	}
}

LevelEnd.prototype.updateState = function (child) {
	for (i in child.children) {
		if (!child.children[i].activated) {
			if (child.isopen) {
				this.close(child);
			}
			return;
		}
	}

	if (child.isopen === false) {
		this.open(child);
	}
}