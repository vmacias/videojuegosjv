function Weapons(game, sprite, actions, bulletLeftColor, bulletRightColor) {
	this.game    = game;
	this.sprite  = sprite;
	this.actions = actions;

	this.fireRate = 500;
	this.lastfire_left  = 0;
	this.lastfire_right = 0;

	this.bulletLeftColor  = bulletLeftColor;
	this.bulletRightColor = bulletRightColor;
}

Weapons.prototype = {
	create: function () {
		this.weapon_left  = this.createWeapon(this.bulletLeftColor);
		this.weapon_right = this.createWeapon(this.bulletRightColor);
	},

	update: function () {
		var now = this.game.time.now;

		if (this.actions.fireleft.isDown && now > this.lastfire_left+this.fireRate) {
			this.lastfire_left = now;
			this.weapon_left.fire();
			this.game.sounds.play_game_sound('weapon_fire_left');
		} else if (this.actions.fireright.isDown && now > this.lastfire_right+this.fireRate) {
			this.lastfire_right = now;
			this.weapon_right.fire();
			this.game.sounds.play_game_sound('weapon_fire_right');
		}
	},

	createWeapon: function (color) {
		var weapon = this.game.add.weapon(1, 'bullet', 1);
		weapon.bullets.children[0].tint = color;
		weapon.bulletKillType  = Phaser.Weapon.KILL_WORLD_BOUNDS;
		weapon.bulletSpeed     = 800;
		weapon.fireRate        = this.fireRate;
		weapon.trackSprite(this.sprite, 45, -2, true);
		return weapon;
	},

	debug: function(x,y,debugBodies) {
		this.weapon_left.debug(x,y,debugBodies);
		this.weapon_right.debug(x+500, y, debugBodies);
	},
}